logonuser="mnsuser"
logonpassword="password"

sudo yum -y update
sudo yum install vim zip unzip git wget -y

### Enabling users and password (need to find the exact line)
sudo sed -i -e 's|#PasswordAuthentication yes|PasswordAuthentication yes|' /etc/ssh/sshd_config
sudo sed -i -e 's|#PasswordAuthentication no|PasswordAuthentication yes|' /etc/ssh/sshd_config
sudo sed -i -e 's|PasswordAuthentication no|PasswordAuthentication yes|' /etc/ssh/sshd_config

#prevent root login

sudo tee -a /etc/ssh/sshd_config >/dev/null <<EOF
PermitRootLogin no
EOF



sudo service sshd restart

#creating new user
sudo adduser ${logonuser}
echo -e "$logonpassword\n$logonpassword\n" | sudo passwd $logonuser

#adding users to sudo file

sudo tee -a /etc/sudoers >/dev/null <<EOF
## add user to /etc/sudoers file 
${logonuser}      ALL=(ALL)       ALL
EOF

sudo tee /etc/yum.repos.d/docker.repo <<-EOF
[dockerrepo]
name=Docker Repository
baseurl=https://yum.dockerproject.org/repo/main/centos/7
enabled=1
gpgcheck=1
gpgkey=https://yum.dockerproject.org/gpg
EOF


#yum installs
sudo yum install -y docker

sudo wget http://repos.fedorapeople.org/repos/dchen/apache-maven/epel-apache-maven.repo -O /etc/yum.repos.d/epel-apache-maven.repo
sudo sed -i s/\$releasever/6/g /etc/yum.repos.d/epel-apache-maven.repo
sudo yum install -y apache-maven


