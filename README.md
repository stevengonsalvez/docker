## Commands for MQ container

sudo docker build -t sapqm .
sudo docker run --env LICENSE=accept --env MQ_QMGR_NAME=C1SAPQM1 --publish 1414:1414 --name SAPQM --detach sapqm


## Commands for the mule container
```
sudo docker build -t muledweet .
```

## Broker containers 

```
docker run --name IBM_MessageBroker_1 -e LICENSE=accept -e NODENAME=dweeter -p 5414:4414 -p 7800:7800 -P --name SAPBROKER --detach iibv10image
```

### creating execution group

``` 
docker exec -it `docker ps | grep IBM_MessageBroker_1 | awk {'print $1'}` /bin/bash -c "mqsicreateexecutiongroup dweeter -e dweet"
```


### Installing scope

```
sudo curl -L git.io/scope -o /usr/local/bin/scope
sudo chmod a+x /usr/local/bin/scope
scope launch --service-token=m8bx3oxgretskumjo8ezzbgxy1rqtx14
```



### Mule container
first 

``` 
git clone https://github.com/stevengonsalvez/dweeter.git
```

```

sudo docker build --build-arg VERSION=1.0 --build-arg NEXUS=54.85.196.177  -t some_dweeter .
sudo docker run --env THANG=imminent-whip --env ELASTIC_HOST=54.85.196.177 --env ELASTIC_PORT=9200 --name MNS_ESB -d muledweet

```

### Running elasticsearch
```
docker run -p 9200:9200 -p 9300:9300 --name mns_elastic -d elasticsearch elasticsearch -Des.node.name="TestNode"
```


#### Running Kibana
```
docker run --name mns-kibana -e ELASTICSEARCH_URL=http://localhost:9200 -p 5601:5601 -d kibana

docker run --name mns-kibana --link mns_elastic:elasticsearch -p 5601:5601 -d kibana

```


### Running Nexus 

```
docker run -d -p 8081:8081 --name nexus sonatype/nexus:oss

```


### Install docker compose

```
#enabling epel release
wget http://dl.fedoraproject.org/pub/epel/7/x86_64/e/epel-release-7-7.noarch.rpm
rpm -ivh epel-release-7-7.noarch.rpm


sudo yum install epel-release
sudo yum install -y python-pip
sudo pip install docker-compose

sudo yum upgrade python*
```


## alternate docker compose install

```
curl -L https://github.com/docker/compose/releases/download/1.8.0/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
ln -s /usr/local/bin/docker-compose /bin/docker-compose









docker-compose --version

```


### loggly

```
docker run --name logspout -d --volume=/var/run/docker.sock:/var/run/docker.sock \
    -e SYSLOG_STRUCTURED_DATA="a2a43510-61c1-4917-8c60-f6b884a29dc5@41058 tag=\"dweeter\"" \
    gliderlabs/logspout \
    syslog+tcp://logs-01.loggly.com:514

```


#### check security of containers

```
git clone https://github.com/docker/docker-bench-security.git
cd docker-bench-security
docker-compose run --rm docker-bench-security

```
